$(window, document, undefined).ready(function() {

  $('input').blur(function() {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
    $(this).removeClass('is-active');
  });

  $('#loginform').submit(function (event) {
    event.preventDefault();

    showMeYourCookies('At loginform submission');

    var cookie = JSON.parse($.cookie('helloween'));
    var data = 'username=' + $('#username').val() + '&password=' + $('#password').val();
    $.ajax({
      data: data,
      timeout: 1000,
      type: 'POST',
      url: '/api/login'

    }).done(function(data, textStatus, jqXHR) {
      showMeYourJqXHR('When loginform is done', jqXHR);
      showMeYourCookies('When loginform is done');

      //window.location = cookie.url;

    }).fail(function(jqXHR, textStatus, errorThrown) {
      //showMeYourJqXHR('When loginform fails', jqXHR);
      //showMeYourCookies('When loginform fails');
    
        if (jqXHR.status === 401) { // HTTP Status 401: Unauthorized
            var preLoginInfo = JSON.stringify({method: 'GET', url: '/'});
            $.cookie('restsecurity.pre.login.request', preLoginInfo);
            window.location = '/login.html';
 
        } else {
            alert('Houston, we have a problem...');
        }   

    });
  });  

});